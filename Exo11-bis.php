<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>    

<?php

// $nb est le nombre à trouver. $coup est le nombre de coup qu'il à fallu pour trouvé $nb
// Ecrire une boucle qui affiche le nombre aléatoire donné par $x et qui affiche
//  le nombre de coup qu'il à fallu pour tombé sur $nb
// Résultat : 
// 3
// 8
// 9
// 9 trouvé en 3 coups

?>
<!-- écrire le code après ce commentaire -->
<?php


$nb = 5;
$coup = 0; //compteur
echo 'Nombre à trouver : ' . $nb . '<br>' . '<br>';

    $x =1;

    while ($x !== $nb) {
        $x = rand(1,5);
        echo $x.'<br>';
        $coup++;
        
    }echo $x . ' trouvé en ' . $coup . ' coups';


?> 
    <!-- écrire le code avant ce commentaire -->

</body>
</html>