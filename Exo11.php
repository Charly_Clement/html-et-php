<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
    <style>
    
        #container {
            height: 500px;
            width: 80%;
            margin: 0 auto;
            background-color: green;
        }
        #graph{
            width: 50px;
            background-color: skyblue;
            content: " ";
        }
    
    </style>
</head>    

    <?php
    
    // Afficher le pourcentage de $b par rapport à $valMaxi avec un chiffre aprés la virgule
    // $b = 966 soit 12.9% de 7500
    
    $b = rand(0,7500);
    $x = 7500;
    $i = ($b / $x * 100);
    ?>
   
    <!-- écrire le code après ce commentaire -->
    
    <?php
        echo '$b = ' . $b . ' soit '. (round ($i,1)) . ' de ' . $x;    
    
    ?>
    
    <!-- écrire le code avant ce commentaire -->

</body>
</html>

