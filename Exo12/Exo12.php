<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
    <link href="https://fonts.googleapis.com/css?family=Public+Sans&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="styleexo12.css">
</head>    
</body>
<style>
    body {
      background-color: #E6E9ED;
    }
    h2 {
      color: white;
    }
    section {
      display: flex;
      width: 80%;
      margin: auto;
    }
    div {
      background-color: white;
      width: 400px;
      height: 200px;
      border-radius: 10px;
      text-align: center;
    }
    .div_center {
      margin: 0 158px;
    }
    .nb_camion {
      width: 80%;
      height: 60px;
      margin: auto;
      margin-top: 15px
    }
    img {
      width: 50px;
      height: 50px;
    }
    table {
      width: 80%;
      margin: auto;
      margin-top: 15px;
      background-color: white;
    }
    th {
      border-bottom: 50px solid grey;
    }
    tr {
      height: 40px;
    }
    tr:hover {
      background-color: grey;
    }
    td {
      text-align: center;
      padding: 20px;
    }
        
    
  </style>

  <?php

  /* Simuler le tableau de bord d'une déchetterie grâce à la puissance de calcul de PHP et l'affichage du CSS.
  Ici, votre tableau de bord, permettera de visualisé les stocks de dechets ( en tonne ) de la com com.
  Trois catégories dans le tri : cartons, déchets verts, mobilier. La fonction rand affectera une quantité
  aléatoire aux déchetteries du réseaux, à vous de faire en sorte que l'on sache qui à quoi et combien cela
  represente en tout. Préciser en nombre de camion(s) le volume que cela represente. Un camion = 30 tonnes.
  Police utiliser : Public Sans sur Google Font, effet de ligne plus grise au survol de la ligne 
  */
  $lev = "Loir en vallée";
  $lcsll = "La Chartre-sur-le Loir";
  $bsd = "Beaumont-sur-Dême";
  $m = "Marçon";
  $c = "Chahaignes";
  $l = "Lhomme";

  $carton = rand(0,20);
  $vert = rand(0,20);
  $mobilier = rand(0,20);

  $nb_camion_carton = 0;
  $nb_camion_vert = 0;
  $nb_camion_mobilier = 0;
  
  $total_camion = $nb_camion_carton + $nb_camion_vert + $nb_camion_mobilier;

  ?>

  <!-- écrire le code après ce commentaire -->

  <h1>SICTOM de Montoire / La Chartre</h1>

  <h2>Quantité total de déchets dans la Communauté de communes</h2> 
  <section>
    <div>
      <img src="carton.svg" alt="carton">
      <p><?php echo ($carton * 7) ?></p>
      <p><?php echo 'Besoin de ' . $nb_camion_carton  .  ' camion(s)' ?></p>
    </div>
    <div class="div_center">
      <img src="vert.svg" alt="vert">
      <p><?php echo ($vert * 7) ?></p>
      <p><?php echo 'Besoin de ' . $nb_camion_vert  .  ' camion(s)' ?></p>
    </div>
    <div>
      <img src="mobilier.svg" alt="mobilier">
      <p><?php echo ($mobilier * 7) ?></p>
      <p><?php echo 'Besoin de ' . $nb_camion_mobilier  .  ' camion(s)' ?></p>
    </div>
  </section>

  <div class="nb_camion">
    <p><?php echo 'Nombre de camion(s) total à prévoir: ' . $total_camion ?></p>
  </div>
    
  <table cellspacing="0">
    <tr>
      <th>Sites de collecte</th>
      <th><img src="carton.svg" alt="carton"></th>
      <th><img src="vert.svg" alt="vert"></th>
      <th><img src="mobilier.svg" alt="mobilier"></th>
    </tr>
    <tr>
      <td><?php echo $lev ?></td>
      <td><?php echo $carton ?></td>
      <td><?php echo $vert ?></td>
      <td><?php echo $mobilier ?></td>
    </tr>
    <tr>
      <td><?php echo $lcsll ?></td>
      <td><?php echo $carton ?></td>
      <td><?php echo $vert ?></td>
      <td><?php echo $mobilier ?></td>
    </tr>
    <tr>
      <td><?php echo $bsd ?></td>
      <td><?php echo $carton ?></td>
      <td><?php echo $vert ?></td>
      <td><?php echo $mobilier ?></td>
    </tr>
    <tr>
      <td><?php echo $m ?></td>
      <td><?php echo $carton ?></td>
      <td><?php echo $vert ?></td>
      <td><?php echo $mobilier ?></td>
    </tr>
    <tr>
      <td><?php echo $c ?></td>
      <td><?php echo $carton ?></td>
      <td><?php echo $vert ?></td>
      <td><?php echo $mobilier ?></td>
    </tr>
    <tr>
      <td><?php echo $l ?></td>
      <td><?php echo $carton ?></td>
      <td><?php echo $vert ?></td>
      <td><?php echo $mobilier ?></td>
    </tr>
  </table>
  <?php

  
  ?>
    <!-- écrire le code avant ce commentaire -->

</body>
</html>

