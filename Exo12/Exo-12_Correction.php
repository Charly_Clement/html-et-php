<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
    <link href="https://fonts.googleapis.com/css?family=Public+Sans&display=swap" rel="stylesheet">
    <style>
    body {
    width: 95%;
    margin: 0 auto;
    background-color: #E6E9ED;
}

h1 { 
    font-family: 'Public Sans', sans-serif; 
    font-size: 40px;
}

h2 {
    font-family: 'Public Sans', sans-serif; 
    color: white;
    font-size: 40px;
    margin-top: -30px;
}

p {
    font-family: "Arial";
    font-size: 23px;
}

.container {
    width: 100%;
    display: flex;
    justify-content: space-around;
    text-align : center;
}

.bloc {
    width: 23%;
    border-radius: 10px;
    box-shadow: 0 0 5px grey;
    padding-top: 25px;
    background-color:#F5F7FA;
}

.bloc img {
    width: 50px;
    height: 50px;
}

#bloc-long {
    width: 87.5%;
    border-radius: 10px;
    box-shadow: 0 0 5px grey;
    padding: 15px;
    background-color:#F5F7FA;
}

table {
    margin: 0 auto 20px auto;
    font-family: 'Public Sans', sans-serif;
    width: 90%;
    text-align: center;
    border-radius: 10px;
    box-shadow: 0 0 5px grey;
    background-color:#F5F7FA;
}

table tr {
    height: 70px;
}

table tr:hover {
    background-color: lightgrey;
}

table img {
    width: 40px;
    height: 40px;
}
    </style>
</head>    

    <?php
    
      /* 
    *   Simuler le tableau de bord d'une déchetterie grâce à la puissance de calcul de PHP et l'affichage du CSS
    *   Ici, votre tableau de bord, permettera de visualisé les stocks de dechets ( en tonne ) de la communauté de commune
    *   Trois catégories dans le tri : cartons, déchets verts, mobilier. La fonction rand affectera une quantité aléatoire aux  
    *   déchetteries du réseaux, à vous de faire en sorte que l'on sache qui à quoi et combien cela represente en tout. Préciser en 
    *   nombre de camion(s) le volume que cela represente. Un camion = 30 tonnes.
    *   Police utilisée : Public Sans sur Google Font, effet de ligne plus grise au survol de la ligne 
    */
    
    
    $lev = "Loir en vallée";
    $lcsll = "La Chartre-sur-le Loir";
    $bsd = "Beaumont-sur-Dême";
    $m = "Marçon";
    $c = "Chahaignes";
    $l = "Lhomme";
    
    $carton = rand(0,20);
    $vert = rand(0,20);
    $mobilier = rand(0,20);
    
    $totalCarton = $carton * 6;
    $totalVert = $vert * 6;
    $totalMobilier = $mobilier * 6;
    
    $camionCarton = round($totalCarton / 30);
    $camionVert = round($totalVert / 30);   
    $camionMobilier = round($totalMobilier / 30);
    
    $totalCamions = $camionCarton + $camionVert + $camionMobilier;
        
    ?>
   
    <!-- écrire le code après ce commentaire -->
    <h1>SICTOM de Montoire / La Chartre</h1>
    
    <h2>Quantité total de déchets dans la Communauté de communes</h2> 
    
    <div class="container">
        <div class="bloc">
            <img src="img/carton.svg" alt="">
            <p><?php echo $totalCarton;?></p>
            <p>Besoin de <?php echo $camionCarton;?> camion(s)</p>
        </div>
        <div class="bloc">
            <img src="img/vert.svg" alt="">
            <p><?php echo $totalVert;?></p>
            <p>Besoin de <?php echo $camionVert;?> camion(s)</p>
        </div>
        <div class="bloc">
            <img src="img/mobilier.svg" alt="">
            <p><?php echo $totalMobilier;?></p>
            <p>Besoin de <?php echo $camionMobilier;?> camion(s)</p>
        </div>
    </div>
    
    <div class="container">
        <p id="bloc-long">Nombre de camions total à prévoir : <?php echo $totalCamions; ?></p>
    </div>
    
    <table cellpadding="5" cellspacing=0>
        <thead>
            <tr>
                <td style="width:40%;">Sites de collecte</td>
                <td><img src="img/carton.svg" alt=""></td>
                <td><img src="img/vert.svg" alt=""></td>
                <td><img src="img/mobilier.svg" alt=""></td>
            </tr>
        </thead>
        <tbody>
            <tr style="height:1px; background-color:darkgrey;">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
               <tr>
                <td><?php echo $lev;?></td>
                <td><?php echo $carton;?></td>
                <td><?php echo $vert;?></td>
                <td><?php echo $mobilier;?></td>
            </tr>
            <tr>
                <td><?php echo $lcsll;?></td>
                <td><?php echo $carton;?></td>
                <td><?php echo $vert;?></td>
                <td><?php echo $mobilier;?></td>
            </tr>
            <tr>
                <td><?php echo $bsd;?></td>
                <td><?php echo $carton;?></td>
                <td><?php echo $vert;?></td>
                <td><?php echo $mobilier;?></td>
            </tr>
            <tr>
                <td><?php echo $m;?></td>
                <td><?php echo $carton;?></td>
                <td><?php echo $vert;?></td>
                <td><?php echo $mobilier;?></td>
            </tr>
            <tr>
                <td><?php echo $c;?></td>
                <td><?php echo $carton;?></td>
                <td><?php echo $vert;?></td>
                <td><?php echo $mobilier;?></td>
            </tr>
            <tr>
                <td><?php echo $l;?></td>
                <td><?php echo $carton;?></td>
                <td><?php echo $vert;?></td>
                <td><?php echo $mobilier;?></td>
            </tr>
        </tbody>
    </table>
    
    <br>
    <!-- écrire le code avant ce commentaire -->

</body>
</html>

