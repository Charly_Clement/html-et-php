<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>    

  <?php


  // $x choisit un chiffre aléatoire entre 0 et 10. Si $x vaut 3 afficher Gain : 300 € si $x vaut 0 afficher banqueroute !
  // /!\ ATTENTION /!\ dans les echo, seul des variables sont admises 
  // Exemple : echo $x, $y, $z; = Gain : 300 €

  $x = rand(0,10);
  $y = "x vaut ";
  $z = "Gain = $x" . "00";
  ?>

  <!-- écrire le code après ce commentaire -->

  <?php 

    echo $x . "<br>";

    if ($x > 0) {
      echo $z;
    }else {
      echo "Banqueroute";
    }


  ?>

  <!-- écrire le code avant ce commentaire -->

</body>
</html>

