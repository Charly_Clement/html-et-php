<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>    

    <?php
// Trouver comment afficher PHP dans la variable $e ( sans écrire "PHP" )
// Faites une condition pour que la chaines de caractères "PHP" soit vérifié et que sont type soit vérifié aussi

    $v = 333;
    $w = "C++";
    $x = "PHP";
    $y = "HTML";
    $z = "CSS";
    
    $a = "$v" . "$w"; // 333 C++
    $b = "$x" . "$y"; // PHP HTML
    $c = "$y" . "$z"; // HTML CSS
    
    $d = "$a" . "$b" . "$c";
    $e = $x;
    
    ?>
   
    <!-- écrire le code après ce commentaire -->
    <?php
    
        if ($e === "PHP") {
            echo "chaine de caractères vérifié";
        }
    
    ?>
    <!-- écrire le code avant ce commentaire -->

</body>
</html>

